# WebChat

A simple chat application with the following requirements:

  - The user should enter a name to login to the system. (Important: no password or registration is required).
  - There is a button to logout after the user logs in.
  - When the user logs in, the last 15 messages will be shown (if available).
  - The system shows a list of all available users.
  - Each user has a textbox to type the message. Pressing Enter will send it.
  - Show "(is typing)" next to the names of users currently typing their messages.
