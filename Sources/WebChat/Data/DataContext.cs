﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using Newtonsoft.Json;

namespace WebChat.Data
{
    public class DataContext : DbContext
    {
        public DataContext()
            : base("name=DataContext")
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<DataContext>());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Connection> Connections { get; set; }
        public DbSet<Message> Messages { get; set; }
    }

    public abstract class Entity
    {
        private Guid _guid = System.Guid.NewGuid();

        [Key]
        [Required]
        public Guid Guid
        {
            get { return _guid; }
            set { _guid = value; }
        }
    }

    public class Message : Entity
    {
        [Required]
        public virtual User Author { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime SentOn { get; set; }
    }

    public class User
    {
        [Key]
        public string UserName { get; set; }
        [JsonIgnore]
        public virtual ICollection<Connection> Connections { get; set; }
    }

    public class Connection
    {
        public string ConnectionId { get; set; }
        public string UserAgent { get; set; }
        public bool Connected { get; set; }
    }
}