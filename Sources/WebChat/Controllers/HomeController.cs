﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace WebChat.Controllers
{
    public class HomeController : Controller
    {
       public ActionResult Index()
       {
           return RedirectToAction("Chat");
       }

        [Authorize]
        public ActionResult Chat()
        {
            return View();
        }
    }
}