﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Ajax.Utilities;

namespace WebChat.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Login")]
        public ActionResult LoginPost(string username, string returnUrl)
        {
            

            FormsAuthentication.SetAuthCookie(username, true);
            if (!returnUrl.IsNullOrWhiteSpace())
            {
                return Redirect(returnUrl);
            }
            return Redirect(FormsAuthentication.DefaultUrl);
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}
