﻿using System.Threading.Tasks;
using WebChat.Data;

namespace WebChat.Hubs
{
    public interface IChatClient
    {
        Task DisplayMessage(Message message);
        Task NotifyMessageDelivered(Message message);
        Task ShowUserIsTyping(User user, bool typing);
        Task UserJoinedChat(User user);
        Task UserLeftChat(User user);
        Task Disconnect(string reason);
    }
}