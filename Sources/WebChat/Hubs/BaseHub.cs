﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using WebChat.Data;
using Microsoft.AspNet.SignalR;

namespace WebChat.Hubs
{
    public abstract class BaseHub<T> : Hub<T> where T : class
    {
        protected readonly DataContext db = new DataContext();

        protected User GetUserInfo()
        {
            var userName = Context.User.Identity.Name;
            var user = db.Users.Include(x => x.Connections)
                .SingleOrDefault(u => u.UserName == userName && u.Connections.Any(c => c.Connected));
            return user;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}