﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Hubs;
using WebChat.Data;
using Microsoft.AspNet.SignalR;

namespace WebChat.Hubs
{
    [Authorize]
    public class ChatHub : BaseHub<IChatClient>
    {
        public void JoinChat()
        {
            var user = SaveUserContext(Context);
            if (user.Connections.Count(x => x.Connected) == 1)
            {
                Clients.Others.UserJoinedChat(user);
            }
        }

        public void LeaveChat()
        {
            var user = GetUserInfo();
            if (user == null) return;

            RemoveUserConnection(Context.ConnectionId);
            if (user.Connections.Count(x => x.Connected) == 0)
            {
                Clients.Others.UserLeftChat(user);
            }
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            LeaveChat();
            return base.OnDisconnected(stopCalled);
        }

        public async Task SendMessage(Message message)
        {
            //Thread.Sleep(200);

            message.Author = GetUserInfo();
            message.SentOn = DateTime.Now;

            db.Messages.Add(message);
            db.SaveChanges();

            await Clients.Others.DisplayMessage(message);
            Clients.Caller.NotifyMessageDelivered(message);
        }

        public void NotifyIamTyping(bool isTyping)
        {
            var user = GetUserInfo();
            Clients.Others.ShowUserIsTyping(user, isTyping);
        }

        public User WhoAmI()
        {
            return GetUserInfo();
        }

        public User[] GetAvailableUsers()
        {
            var users = db.Users.Where(user => user.Connections.Any(c => c.Connected)).ToArray();
            return users;
        }

        public Message[] GetLastMessages(int amount)
        {
            amount = Math.Max(0, Math.Min(amount, 100)); // range 0..100
            var messages = db.Messages.OrderByDescending(x => x.SentOn).Take(amount).ToArray();
            return messages.Reverse().ToArray();
        }

        private User SaveUserContext(HubCallerContext context)
        {
            var userName = context.User.Identity.Name;
            var user = db.Users.Include(x => x.Connections).SingleOrDefault(x => x.UserName == userName);
            if (user == null)
            {
                user = new User { UserName = userName, Connections = new List<Connection>() };
                db.Users.Add(user);
            }

            var connection = user.Connections.SingleOrDefault(x => x.ConnectionId == context.ConnectionId);
            if (connection == null)
            {
                connection = new Connection
                {
                    ConnectionId = context.ConnectionId,
                    UserAgent = context.Headers["User-Agent"],
                    Connected = true,
                };
                user.Connections.Add(connection);
            }
            else
            {
                connection.UserAgent = context.Headers["User-Agent"];
                connection.Connected = true;
            }

            db.SaveChanges();

            return user;
        }

        private void RemoveUserConnection(string connectionId)
        {
            var connection = db.Connections.Find(connectionId);
            db.Connections.Remove(connection);
            db.SaveChanges();
        }
    }
}
