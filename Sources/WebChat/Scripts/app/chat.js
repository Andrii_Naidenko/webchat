﻿///<reference path="../_reference.js"/>

(function ($) {
    var app = angular.module('chatApp', ['SignalR']);

    app.controller('HeaderController', function ($scope, $location, ChatEvents) {
        $scope.signout = function ($event) {
            ChatEvents.disconnectHub().then(function () {
                var href = $($event.target).attr('href');
                $location.url(href);
            });
            $event.stopPropagation();
        };
    });

    app.controller('ChatController', function ($scope, Hub, ChatEvents, $q) {
        $scope.contacts = [];
        $scope.messages = [];
        $scope.inputMessage = '';
        $scope.iam = null;
        $scope.loading = true;

        $scope.typing = function (contacts) {
            return contacts.filter(function (c) { return c.isTyping; });
        };

        var chat = $scope.chat = new Hub('ChatHub', {
            //logging: true,
            listeners: {
                'userJoinedChat': function (user) {
                    addContact(new User(user));
                    $scope.$digest();
                },
                'userLeftChat': function (user) {
                    removeContact(new User(user));
                    $scope.$digest();
                },
                'disconnect': function (reason) {
                    console.log('Disconnecting, reason: ' + reason);
                    chat.disconnect();
                },
                'displayMessage': function (message) {
                    $scope.displayMessage(new Message(message));
                    $scope.$digest();
                },
                'notifyMessageDelivered': function (message) {
                    addOrUpdate(new Message(message));
                    $scope.$digest();
                },
                'showUserIsTyping': function (user, isTyping) {
                    user = new User(user);
                    var contact = $scope.contacts.filter(function (u) { return u.name == user.name; })[0];
                    if (contact) {
                        contact.isTyping = isTyping;
                        $scope.$digest();
                    }
                }
            },
            methods: ['joinChat', 'leaveChat', 'whoAmI', 'sendMessage', 'getAvailableUsers', 'getLastMessages', 'notifyIamTyping']
        });

        $scope.$watch('inputMessage', $.throttle(1000, function (newValue) {
            chat.notifyIamTyping(!!newValue);
        }));

        $scope.sendMessage = function () {
            if (!$scope.inputMessage.trim()) return;
            var message = new Message({
                Text: $scope.inputMessage,
                author: $scope.iam,
                status: 'sending'
            });
            $scope.displayMessage(message);
            chat.sendMessage(message);
            $scope.inputMessage = '';
        };

        $scope.displayMessage = function (message) {
            if (!message) return;
            if (!(message.text || '').trim()) return;

            if (message.author) {
                var contact = $scope.contacts.filter(message.author.comparer())[0];
                if (contact) {
                    contact.isTyping = false;
                }
            }

            $scope.messages.push(message);
        };

        ChatEvents.onDisconnectHub($scope, function () {
            var q = $q.defer();
            $(chat.connection).on('onDisconnect', q.resolve);
            chat.disconnect();
            return q.promise;
        });

        $scope.displayMessage(new Message({ Text: 'Please wait, connecting to the chat...' }));
        chat.promise.done(function() {
            chat.joinChat().then(init);
        });

        // PRIVATE FUNCTIONS

        function init() {
            $q.all([
                chat.whoAmI(),
                chat.getAvailableUsers(),
                chat.getLastMessages(15)
            ]).then(function (results) {
                $scope.iam = new User(results[0]);
                angular.forEach(results[1], function (data) { addContact(new User(data)); });
                angular.forEach(results[2], function (data) { $scope.displayMessage(new Message(data)); });
                $scope.displayMessage(new Message({ Text: 'You are connected to the chat...' }));
                $scope.loading = false;
            });
        }

        function addContact(user) {
            removeContact(user);
            $scope.contacts.push(user);
        }

        function removeContact(user) {
            var lookupContact = $scope.contacts.filter(function (u) { return u.name == user.name; })[0];
            if (lookupContact) {
                $scope.contacts.splice($scope.contacts.indexOf(lookupContact), 1);
            }
        }

        function addOrUpdate(message) {
            var lookupMessage = $scope.messages.filter(function (msg) { return msg.guid == message.guid; })[0];
            if (lookupMessage) {
                lookupMessage.status = 'sent';
            } else {
                $scope.displayMessage(message);
            }
        }
    });

    app.factory('PubSub', function ($rootScope, $q) {
        return {
            publish: function (eventName, args) {
                var defer = $q.defer();
                var eventArgs = {
                    callback: defer.resolve,
                    args: Array.prototype.slice.call(arguments, 1)
                };
                $rootScope.$broadcast(eventName, eventArgs);
                return defer.promise;
            },
            subscribe: function (eventName, scope, handler) {
                scope.$on(eventName, function (event, eventArgs) {
                    $q.when(handler.apply(this, eventArgs.args))
                        .then(eventArgs.callback);
                });
            }
        };
    });

    app.factory('ChatEvents', function (PubSub) {
        return {
            disconnectHub: function () {
                PubSub.publish('disconnectHub');
            },
            onDisconnectHub: function (scope, handler) {
                PubSub.subscribe('disconnectHub', scope, handler);
            }
        };
    });

    app.directive('scrollBottom', function () {
        function scrollBottom(element) {
            var scrollHeight = $(element)[0].scrollHeight;
            $(element).scrollTop(scrollHeight);
        }

        function handleScrollBottom(newValue) {
            if (newValue) {
                scrollBottom(this);
            }
        }

        return {
            scope: {
                scrollBottom: "="
            },
            link: function (scope, element) {
                if (scope.scrollBottom instanceof Array) {
                    for (var i = scope.scrollBottom.length - 1; i >= 0; i--) {
                        scope.$watchCollection('scrollBottom[' + i + ']', handleScrollBottom.bind(element));
                    }
                } else {
                    scope.$watchCollection('scrollBottom', handleScrollBottom.bind(element));
                }
            }
        };
    });

    app.directive('autofocus', function ($timeout) {
        return {
            scope: {
                trigger: '@autofocus'
            },
            link: function (scope, element) {
                scope.$watch('trigger', function (value) {
                    console.log('focus to element ' + element[0].id + " " + value);
                    if (value == 'true') {
                        $timeout(function () {
                            console.log('giving focus to element', element[0].id);
                            element[0].focus();
                        });
                    }
                });
            }
        };
    });

})(jQuery);
