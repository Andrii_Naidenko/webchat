﻿///<reference path="../_reference.js"/>

(function () {
    function Message(data) {
        data = data || {};
        this.guid = data.Guid || guid();
        this.text = data.Text;
        this.author = data.author || data.Author && new User(data.Author);
        this.status = data.status;
    }

    function User(data) {
        data = data || {};
        this.name = data.UserName;
        this.isTyping = false;
    }

    User.prototype.is = function (user) {
        return user && this.name == user.name;
    };

    User.prototype.comparer = function() {
        return this.is.bind(this);
    };

    window.Message = Message;
    window.User = User;

})();
